This project contains three basic functionalities but I break them into the user stories so I can easily write codes.
1.	Create a basic application with a three-button navigation view
2.	Build a data model for basic authentication and get the required methods for search banks, validation for Iban, Validation for BIC validation for the postcode.
3.	Create a UI for each fragment for three basics part of the application: Search banks, validation Iban and BIC, validation Postcode
4.	Build a ViewModel for Search banks
5.	Build a ViewModel for IBAN
6.	Build a ViewModel for BIC
7.	Build a ViewModel for Postcode
8.	Use ViewModels to send data to UI
After writing code for each module of the project, with Espresso library I write test cases for each part of the project. Also, I created one test suit so you could run all test cases together.

The libraries I used in this project are Retrofit2 and Espresso.
The Retrofit library used for basic auth and get JSON response from the server.
The Espresso library used for write test cases.
Whole project based on MVVM architecture.

