package com.kamali.saito_challenge.Model

import android.util.Base64
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class BankApiService {
    private val BASE_URL = "https://tyre24.alzura.com/de/de/rest/v12/utils/"
    private val credentials = "106901" + ":" + "MTYzYmZkNjZiZmJiMTg2M2IwNjU2Nzk5NzI5OTVjNGY="
    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(BankApi::class.java)
    private val authHeader =
        "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)

    fun getBanks(blz: String): Single<BankInfo> {
        return api.getBanks(authHeader, blz)
    }

    fun validIban(iban: String): Single<ValidIban> {
        return api.validateIBAN(authHeader, iban)
    }

    fun validBic(bic: String): Single<ValidBic> {
        return api.validateBIC(authHeader, bic)
    }

    fun validPostcode(countycode: String,postcode: String): Single<ValidPostcode> {
        return api.validatePostcode(authHeader, countycode,postcode)
    }
}