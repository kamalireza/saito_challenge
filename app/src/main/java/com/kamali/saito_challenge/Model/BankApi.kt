package com.kamali.saito_challenge.Model

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface BankApi {
    @GET("searchBic")
    fun getBanks(@Header("Authorization") authHeader: String, @Query("blz") bankName: String): Single<BankInfo>

    @GET("validateBic")
    fun validateBIC(@Header("Authorization") authHeader: String, @Query("bic") bicName: String): Single<ValidBic>

    @GET("validateIban")
    fun validateIBAN(@Header("Authorization") authHeader: String, @Query("iban") iban: String): Single<ValidIban>

    @GET("validatePostCode")
    fun validatePostcode(@Header("Authorization") authHeader: String,
                         @Query("countryCode") countryCode: String,
                         @Query("postCode") postCode: String): Single<ValidPostcode>

}