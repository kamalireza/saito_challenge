package com.kamali.saito_challenge.Model

data class BankInfo(
    val code: String?,
    val data: Data?
)

class Data(
    val page: Int?,
    val pageCount: Int?,
    val bics: List<Bics>,
    val location: Boolean?,
    val bankName: Boolean?,
    val countryCode: String?,
    val blz: String?,
    val bic: String?

)

data class Bics(
    val id: String?,
    val countryId: String?,
    val bankName: String?,
    val location: String?,
    val blz: String?,
    val bicCode: String?,
    val bicCodeOther: List<String?>,
    val countryCode: String?
)

data class ValidBic(
    val code:String,
    val data:bicData?
)

class bicData(
    val bic:String
)

data class ValidIban(
    val code:String,
    val data:ibanData?
)

class ibanData(
    val iban:String
)

data class ValidPostcode(
    val code:String,
    val data:postcodeData?
)
class postcodeData(
    val postCode:String
)