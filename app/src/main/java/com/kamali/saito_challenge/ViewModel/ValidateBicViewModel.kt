package com.kamali.saito_challenge.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kamali.saito_challenge.Model.BankApiService
import com.kamali.saito_challenge.Model.BankInfo
import com.kamali.saito_challenge.Model.ValidBic
import com.kamali.saito_challenge.Model.ValidIban
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ValidateBicViewModel : ViewModel() {

    private val bankService = BankApiService()
    private val disposable = CompositeDisposable()

    val validateLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val bics = MutableLiveData<ValidBic>()

    fun refresh(bic: String) {
        fetchFromRemote(bic)
    }

    private fun fetchFromRemote(bic: String) {
        loading.value = true
        disposable.add(
            bankService.validBic(bic)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ValidBic>() {
                    override fun onSuccess(validBic: ValidBic) {
                        bics.value = validBic
                        validateLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        validateLoadError.value = true
                        loading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}