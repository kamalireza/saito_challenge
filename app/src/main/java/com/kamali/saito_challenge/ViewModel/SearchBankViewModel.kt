package com.kamali.saito_challenge.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kamali.saito_challenge.Model.BankApiService
import com.kamali.saito_challenge.Model.BankInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class SearchBankViewModel : ViewModel() {

    private val bankService = BankApiService()
    private val disposable = CompositeDisposable()

    val bankLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val banks = MutableLiveData<BankInfo>()


    fun refresh(blz: String = "") {
        fetchFromRemote(blz)
    }

    private fun fetchFromRemote(blz: String) {
        loading.value = true
        disposable.add(
            bankService.getBanks(blz)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<BankInfo>() {
                    override fun onSuccess(bankList: BankInfo) {
                        banks.value = bankList
                        bankLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        bankLoadError.value = true
                        loading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}