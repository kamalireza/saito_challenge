package com.kamali.saito_challenge.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kamali.saito_challenge.Model.BankApiService
import com.kamali.saito_challenge.Model.BankInfo
import com.kamali.saito_challenge.Model.ValidIban
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ValidateIbanViewModel : ViewModel() {

    private val bankService = BankApiService()
    private val disposable = CompositeDisposable()

    val validateLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val ibans = MutableLiveData<ValidIban>()

    fun refresh(iban: String) {
        fetchFromRemote(iban)
    }

    private fun fetchFromRemote(iban: String) {
        loading.value = true
        disposable.add(
            bankService.validIban(iban)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ValidIban>() {
                    override fun onSuccess(validIban: ValidIban) {
                        ibans.value = validIban
                        validateLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        validateLoadError.value = true
                        loading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}