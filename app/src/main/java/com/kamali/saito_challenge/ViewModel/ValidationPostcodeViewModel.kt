package com.kamali.saito_challenge.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kamali.saito_challenge.Model.BankApiService
import com.kamali.saito_challenge.Model.ValidPostcode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ValidationPostcodeViewModel : ViewModel() {

    private val bankService = BankApiService()
    private val disposable = CompositeDisposable()

    val validateLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val posts = MutableLiveData<ValidPostcode>()

    fun refresh(country: String = "",post: String = "") {
        fetchFromRemote(country,post)
    }

    private fun fetchFromRemote(country: String,post: String) {
        loading.value = true
        disposable.add(
            bankService.validPostcode(country,post)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ValidPostcode>() {
                    override fun onSuccess(validpostcode: ValidPostcode) {
                        posts.value = validpostcode
                        validateLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        validateLoadError.value = true
                        loading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}