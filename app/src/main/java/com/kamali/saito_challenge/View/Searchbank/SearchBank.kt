package com.kamali.saito_challenge.View.Searchbank

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kamali.saito_challenge.R
import com.kamali.saito_challenge.ViewModel.SearchBankViewModel

class SearchBank : Fragment() {

    private lateinit var viewModel: SearchBankViewModel

    private lateinit var searchEditText: EditText
    private lateinit var searchButton: Button
    private lateinit var errorTextView: TextView
    private lateinit var loadingProgressBar: ProgressBar

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_bank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchBankViewModel::class.java)

        observeViewModel()

        searchEditText = view.findViewById(R.id.editText_bankName)
        searchButton = view.findViewById(R.id.btn_search);
        errorTextView = view.findViewById(R.id.textView_error)
        loadingProgressBar = view.findViewById(R.id.progress_bar_banks_loading)

        searchButton.setOnClickListener { viewModel.refresh(searchEditText.text.toString()) }

        viewManager = LinearLayoutManager(activity)

        recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_bank_list).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
        }
    }

    private fun observeViewModel() {
        viewModel.banks.observe(this, Observer {
            if (it?.data != null) {
                recyclerView.adapter = BankAdapter(it)
                recyclerView.visibility = VISIBLE
            } else {
                recyclerView.visibility = GONE
            }
        })

        viewModel.bankLoadError.observe(this, Observer {
            errorTextView.visibility = if(it) GONE else VISIBLE
        })

        viewModel.loading.observe(this, Observer {
            if (it) {
                loadingProgressBar.visibility = VISIBLE
                recyclerView.visibility = GONE
            } else {
                loadingProgressBar.visibility = GONE
            }
        })
    }
}