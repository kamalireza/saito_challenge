package com.kamali.saito_challenge.View.ValidatePostcode

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kamali.saito_challenge.R
import com.kamali.saito_challenge.ViewModel.ValidationPostcodeViewModel

class ValidatePostcode : Fragment() {

    private lateinit var PostcodeViewModel: ValidationPostcodeViewModel

    private lateinit var searchPostEditText: EditText
    private lateinit var searchCountryEditText: EditText
    private lateinit var validateButton: Button
    private lateinit var statusTextView: TextView
    private lateinit var loadingProgressBar: ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_validate_postcode, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        PostcodeViewModel = ViewModelProviders.of(this).get(ValidationPostcodeViewModel::class.java)


        observeViewModel()

        //
        searchCountryEditText = view.findViewById(R.id.editText_countryCode)
        searchPostEditText = view.findViewById(R.id.editText_postcode)
        validateButton = view.findViewById(R.id.button_validation_postcode);
        statusTextView = view.findViewById(R.id.textView_validation_result_postcode)
        loadingProgressBar = view.findViewById(R.id.progressBar_postcode)

        validateButton.setOnClickListener {

            PostcodeViewModel.refresh(
                searchCountryEditText.text.trim().toString(),
                searchPostEditText.text.trim().toString()
            )

        }
    }

    private fun observeViewModel() {

        PostcodeViewModel.posts.observe(this, Observer {
            if (it != null) {
                statusTextView.text = it.code
                statusTextView.visibility = View.VISIBLE
            } else {
                statusTextView.visibility = View.GONE
            }
        })

        PostcodeViewModel.loading.observe(this, Observer {
            if (it == true) {
                loadingProgressBar.visibility = View.VISIBLE
            } else {
                loadingProgressBar.visibility = View.GONE
            }
        })

    }
}