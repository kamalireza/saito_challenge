package com.kamali.saito_challenge.View.ValidateBicIban

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kamali.saito_challenge.R
import com.kamali.saito_challenge.ViewModel.ValidateBicViewModel
import com.kamali.saito_challenge.ViewModel.ValidateIbanViewModel
import kotlinx.android.synthetic.main.fragment_validate_iban.*

class ValidateBicIban : Fragment() {

    private lateinit var ibanViewModel: ValidateIbanViewModel
    private lateinit var bicViewModel: ValidateBicViewModel

    private lateinit var searchEditText: EditText
    private lateinit var resultEditText: EditText
    private lateinit var validateButton: Button
    private lateinit var ibanRadioButton: RadioButton
    private lateinit var bicRadioButton: RadioButton
    private lateinit var statusTextView: TextView
    private lateinit var loadingProgressBar: ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_validate_iban, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ibanViewModel = ViewModelProviders.of(this).get(ValidateIbanViewModel::class.java)
        bicViewModel = ViewModelProviders.of(this).get(ValidateBicViewModel::class.java)

        observeViewModel()

        searchEditText = view.findViewById(R.id.editText_iban_bic)
        validateButton = view.findViewById(R.id.button_validation_iban_bic);
        statusTextView = view.findViewById(R.id.textView_validation_result)
        loadingProgressBar = view.findViewById(R.id.progressBar_validation)
        ibanRadioButton = view.findViewById(R.id.radioButton_iban)
        bicRadioButton = view.findViewById(R.id.radioButton_bic)

        validateButton.setOnClickListener {
            if (radioButton_iban.isChecked) {
                ibanViewModel.refresh(searchEditText.text.trim().toString())
            } else {
                bicViewModel.refresh(searchEditText.text.trim().toString())
            }
        }
    }

    private fun observeViewModel() {

        ibanViewModel.ibans.observe(this, Observer {
            if (it != null) {
                statusTextView.text = it.code
                statusTextView.visibility = View.VISIBLE
            } else {
                statusTextView.visibility = View.GONE
            }
        })


        ibanViewModel.loading.observe(this, Observer {
            if (it == true) {
                loadingProgressBar.visibility = View.VISIBLE
            } else {
                loadingProgressBar.visibility = View.GONE
            }
        })


        bicViewModel.bics.observe(this, Observer {
            if (it != null) {
                statusTextView.text = it.code
                statusTextView.visibility = View.VISIBLE
            } else {
                statusTextView.visibility = View.GONE
            }
        })


        bicViewModel.loading.observe(this, Observer {
            if (it == true) {
                loadingProgressBar.visibility = View.VISIBLE
            } else {
                loadingProgressBar.visibility = View.GONE
            }
        })
    }

}