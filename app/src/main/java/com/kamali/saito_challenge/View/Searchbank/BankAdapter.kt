package com.kamali.saito_challenge.View.Searchbank

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kamali.saito_challenge.Model.BankInfo
import com.kamali.saito_challenge.R

class BankAdapter(private val dataSet: BankInfo) :
    RecyclerView.Adapter<BankAdapter.BankViewHolder>() {

    override fun onBindViewHolder(holder: BankViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.text_view_bank_name).text =
            dataSet.data?.bics?.get(position)?.bankName ?: ""
        holder.view.findViewById<TextView>(R.id.text_view_location).text =
            dataSet.data?.bics?.get(position)?.location ?: ""
        holder.view.findViewById<TextView>(R.id.text_view_blz).text =
            dataSet.data?.bics?.get(position)?.blz ?: ""
        holder.view.findViewById<TextView>(R.id.text_view_bic_code).text =
            dataSet.data?.bics?.get(position)?.bicCode ?: ""
        holder.view.findViewById<TextView>(R.id.text_view_country_code).text =
            dataSet.data?.bics?.get(position)?.countryCode ?: ""
    }

    override fun getItemCount() = dataSet.data?.bics?.size ?: 0

    class BankViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BankViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.bank_list_item, parent, false)

        return BankViewHolder(view)
    }
}