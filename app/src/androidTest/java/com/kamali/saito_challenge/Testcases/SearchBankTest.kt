package com.kamali.saito_challenge.Testcases

import android.os.SystemClock
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.TypeTextAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.kamali.saito_challenge.R
import com.kamali.saito_challenge.View.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class SearchBankTest {
    @get: Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun test_ViewSearchBankFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_search_bank))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun show_SearchButton_SearchBankFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_search_bank))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.btn_search))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun show_EditViewSearch_SearchBankFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_search_bank))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.editText_bankName))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun invalidSearchBank_SearchBankFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_search_bank))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.editText_bankName)).perform(TypeTextAction("9999"))

        Espresso.onView(ViewMatchers.withId(R.id.btn_search)).perform(click())
        SystemClock.sleep(3000);

        Espresso.onView(withId(R.id.textView_error)).check(ViewAssertions.matches(ViewMatchers.withText("An Error happened")))
    }
}