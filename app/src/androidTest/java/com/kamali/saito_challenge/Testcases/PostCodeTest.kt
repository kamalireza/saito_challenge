package com.kamali.saito_challenge.Testcases

import android.os.SystemClock
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.TypeTextAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.kamali.saito_challenge.R
import com.kamali.saito_challenge.View.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class PostCodeTest {
    @get: Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun test_ViewValidatePostcodeFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun show_SearchButton_ViewValidatePostcodeFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.button_validation_postcode))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun show_EditViewCountryValidation_ViewValidatePostcodeFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.editText_countryCode))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun show_EditViewPostValidation_ViewValidatePostcodeFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.editText_postcode))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun validateCountryPostcode_ViewValidatePostcodeFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.editText_countryCode))
            .perform(TypeTextAction("DE"))
        Espresso.onView(ViewMatchers.withId(R.id.editText_postcode))
            .perform(TypeTextAction("67661"))

        Espresso.onView(ViewMatchers.withId(R.id.button_validation_postcode))
            .perform(ViewActions.click())

        SystemClock.sleep(2000);
        Espresso.onView(ViewMatchers.withId(R.id.textView_validation_result_postcode))
            .check(ViewAssertions.matches(withText("OK")))
    }

    @Test
    fun invalidateCountryPostcode_ViewValidatePostcodeFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.editText_countryCode))
            .perform(TypeTextAction("NL"))
        Espresso.onView(ViewMatchers.withId(R.id.editText_postcode))
            .perform(TypeTextAction("67661"))

        Espresso.onView(ViewMatchers.withId(R.id.button_validation_postcode))
            .perform(ViewActions.click())

        SystemClock.sleep(2000);

        Espresso.onView(ViewMatchers.withId(R.id.textView_validation_result_postcode)).check(
            ViewAssertions.matches(
                withText(("ERR_GEO_INVALID_POST_CODE"))
            )
        )
    }

    @Test
    fun invalidateCountry_ViewValidatePostcodeFragment() {

        Espresso.onView(ViewMatchers.withId(R.id.navigation_validate_postcode))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.editText_countryCode))
            .perform(TypeTextAction("G"))
        Espresso.onView(ViewMatchers.withId(R.id.editText_postcode))
            .perform(TypeTextAction("67661"))

        Espresso.onView(ViewMatchers.withId(R.id.button_validation_postcode))
            .perform(ViewActions.click())

        SystemClock.sleep(3000);

        Espresso.onView(ViewMatchers.withId(R.id.textView_validation_result_postcode)).check(
            ViewAssertions.matches(
                withText(("ERR_GEO_INVALID_COUNTRY_CODE"))
            )
        )
    }

}