package com.kamali.saito_challenge.Testcases

import android.os.SystemClock
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.TypeTextAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.kamali.saito_challenge.R
import com.kamali.saito_challenge.View.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class ValidateIbanBicTest {
    @get: Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun test_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())
        Espresso.onView(withId(R.id.navigation_validate_iban))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun show_SearchButton_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.button_validation_iban_bic))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun show_EditViewValidation_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.editText_iban_bic))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun show_RadioButtonIban_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.radioButton_iban))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun show_RadioButtonBic_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.radioButton_bic))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun validateIban_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.radioButton_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.editText_iban_bic)).perform(TypeTextAction("DE12 5001 0517 0648 4898 90"))

        Espresso.onView(withId(R.id.button_validation_iban_bic)).perform(ViewActions.click())

        SystemClock.sleep(2000);
        Espresso.onView(withId(R.id.textView_validation_result)).check(ViewAssertions.matches(ViewMatchers.withText("OK")))
    }

    @Test
    fun invalidateIban_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.radioButton_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.editText_iban_bic)).perform(TypeTextAction("DE12 5001 0517 0648 4898 50"))

        Espresso.onView(withId(R.id.button_validation_iban_bic)).perform(ViewActions.click())

        SystemClock.sleep(2000);
        Espresso.onView(withId(R.id.textView_validation_result)).check(ViewAssertions.matches(ViewMatchers.withText("ERR_FINANCIAL_INVALID_IBAN")))
    }

    @Test
    fun validateBic_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.radioButton_bic)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.editText_iban_bic)).perform(TypeTextAction("MALADE51KUS"))

        Espresso.onView(withId(R.id.button_validation_iban_bic)).perform(ViewActions.click())

        SystemClock.sleep(2000);
        Espresso.onView(withId(R.id.textView_validation_result)).check(matches(withText(("OK"))))
    }

    @Test
    fun invalidateBic_ViewValidateIbanBicFragment() {

        Espresso.onView(withId(R.id.navigation_validate_iban)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.radioButton_bic)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.editText_iban_bic)).perform(TypeTextAction("MALADE51KOS"))

        Espresso.onView(withId(R.id.button_validation_iban_bic)).perform(ViewActions.click())

        SystemClock.sleep(2000);
        Espresso.onView(withId(R.id.textView_validation_result)).check(matches(withText(("ERR_FINANCIAL_INVALID_BIC"))))
    }


}