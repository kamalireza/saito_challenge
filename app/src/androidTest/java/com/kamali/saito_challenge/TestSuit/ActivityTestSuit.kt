package com.kamali.saito_challenge.TestSuit

import com.kamali.saito_challenge.Testcases.PostCodeTest
import com.kamali.saito_challenge.Testcases.SearchBankTest
import com.kamali.saito_challenge.Testcases.ValidateIbanBicTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    SearchBankTest::class,
    ValidateIbanBicTest::class,
    PostCodeTest::class)
class ActivityTestSuit